"""
Training script 4 Detection
"""
import sys
import os

o_path = os.getcwd()
sys.path.append(o_path)

from dataloaders.mscoco import CocoDetection, CocoDataLoader
# from lib.object_detector import ObjectDetector
from lib.object_detector_roialgin import ObjectDetector
# from lib.object_detector_lsc import ObjectDetector_lsc
from lib.object_detector_stn import ObjectDetector_stn
# from lib.rfn_detector import RFNDetector
import numpy as np
from torch import optim
import torch
import pandas as pd
import time
from config import ModelConfig, FG_FRACTION, RPN_FG_FRACTION, IM_SCALE, BOX_SCALE
from torch.nn import functional as F
from lib.fpn.box_utils import bbox_loss
import torch.backends.cudnn as cudnn
from pycocotools.cocoeval import COCOeval
from lib.pytorch_misc import optimistic_restore, clip_grad_norm
from torch.optim.lr_scheduler import ReduceLROnPlateau
from tqdm import tqdm
import dill as pkl
import json
from torch.autograd import Variable
from lib.focalloss import FocalLoss
import pdb

#os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"  # see issue #152
#os.environ["CUDA_VISIBLE_DEVICES"] = "5"
# cudnn.deterministic =True
# torch.backends.cudnn.deterministic = True
# os.environ["CUDA_VISIBLE_DEVICES"]="2, 3"

cudnn.benchmark = True
conf = ModelConfig()
#conf.test = True
#conf.fcloss = True
#conf.noval = True
# conf.klloss = True
#conf.stn = 1
conf.coco2017 = True
#conf.testdev = True

assert conf.coco2017, "Object detection test only supports coco2017"

set_name = "test-dev2017" if conf.testdev else "test2017"

if conf.stn is not None:
    set_name += "_stn{}".format(conf.stn)

if conf.fcloss:
    set_name += "_fcloss"

if conf.noval:
    set_name += "_noval"

if conf.klloss:
    set_name += "_klloss"
else:
    set_name += "_roialign"
# save_subdir = '{}/detector'.format(set_name)
#
# if not os.path.exists(os.path.join(conf.save_dir, save_subdir)):
#     os.makedirs(os.path.join(conf.save_dir, save_subdir))



#conf.ckpt = '/home/liao/work_code/vehicle_detection/rfn/checkpoints/tmp/coco_rfn/detector/coco-detector-5.tar'
# conf.ckpt = '/home/liao/work_code/vehicle_detection/rfn/checkpoints/tmp/coco_faster_rcnn_stn1_fcloss_noval/detector/coco-detector-9.tar'
#conf.ckpt = '/home/liao/work_code/vehicle_detection/rfn/checkpoints/tmp/coco_faster_rcnn_stn1_fcloss_noval/detector/coco-detector-9.tar'
# conf.ckpt = '/home/liao/work_code/vehicle_detection/rfn/checkpoints/tmp/coco_faster_rcnn_stn3_fcloss_noval_roialign/detector/coco-detector-9.tar'
# conf.ckpt = '/home/liao/work_code/vehicle_detection/rfn/checkpoints/tmp/coco_faster_rcnn_stn0_fcloss_noval_klloss/detector/checkpoints.tar'


test = CocoDetection('test-dev2017' if conf.testdev else 'test2017', conf.coco2017)

test_loader, _ = CocoDataLoader.splits(test, None, batch_size=conf.batch_size,
                                                 num_workers=conf.num_workers,
                                                 num_gpus=conf.num_gpus,)

# if conf.model == 'rfn':
#    detector = RFNDetector(classes=train.ind_to_classes, num_gpus=conf.num_gpus,
#                              mode='rpntrain' if not conf.use_proposals else 'proposals', use_resnet=conf.use_resnet)
# else:

# if conf.lsc:
#    detector = ObjectDetector_lsc(classes=train.ind_to_classes, num_gpus=conf.num_gpus,
#                              mode='rpntrain' if not conf.use_proposals else 'proposals', use_resnet=conf.use_resnet)
if conf.stn is not None:
    detector = ObjectDetector_stn(classes=test.ind_to_classes, num_gpus=conf.num_gpus,
                                  mode='rpntrain' if not conf.use_proposals else 'proposals', use_resnet=conf.use_resnet, thresh=conf.thresh)
else:
    detector = ObjectDetector(classes=test.ind_to_classes, num_gpus=conf.num_gpus,
                              mode='rpntrain' if not conf.use_proposals else 'proposals', use_resnet=conf.use_resnet, thresh=conf.thresh)

# Note: if you're doing the stanford setup, you'll need to change this to freeze the lower layers
if conf.use_proposals:
    for n, param in detector.named_parameters():
        if n.startswith('features'):
            param.requires_grad = False

optimizer = optim.SGD([p for p in detector.parameters() if p.requires_grad],
                      weight_decay=conf.l2, lr=conf.lr * conf.num_gpus * conf.batch_size, momentum=0.9)
scheduler = ReduceLROnPlateau(optimizer, 'max', patience=3, factor=0.1,
                              verbose=True, threshold=0.001, threshold_mode='abs', cooldown=1)

start_epoch = 0
if conf.ckpt is not None:
    ckpt = torch.load(conf.ckpt)
    if optimistic_restore(detector, ckpt['state_dict']):
        optimizer.load_state_dict(ckpt['optimizer'])
        start_epoch = ckpt['epoch']
        # optimizer = ckpt['optimizer']
        print("=> loaded checkpoint '{} for test' (train for {} epochs)".format(conf.ckpt, conf.num_epochs))
        del ckpt
    # else:
    #     raise Exception('{} DONOT match current model'.format(conf.ckpt))
else:
    raise Exception('A checkpoint muss be assgined for test')

detector.cuda()


def val_epoch():
    detector.eval()
    # all_boxes is a list of length number-of-classes.
    # Each list element is a list of length number-of-images.
    # Each of those list elements is either an empty list []
    # or a numpy array of detection.
    cache_path = os.path.join(conf.cache, set_name)
    if not os.path.exists(cache_path):
        os.makedirs(cache_path)

    cache_file = os.path.join(cache_path, 'epoch{}_{}.json'.format(start_epoch,"test-dev2017" if conf.testdev else "test2017"))
    vr = []
    with tqdm(total=test_loader.__len__()) as pbar:
        for val_b, batch in enumerate(test_loader):
            instance_results = val_batch(val_b, batch)
            for i, img_id in enumerate(instance_results["image_id"]):
                instance = {"image_id": batch.image_ids[img_id],
                            "category_id": instance_results["category_id"][i].item(),
                            "bbox": instance_results["bbox"][i].tolist(),
                            "score": instance_results["score"][i].item()}
                vr.append(instance)
            pbar.update(1)
            # if val_b%20==0 and val_b>0:
            #     print("test")
            #     pdb.set_trace()
            #     with open(cache_file, 'w') as f:
            #         json.dump(vr, f)

    # vr = np.concatenate(vr, 0)
    if os.path.exists(cache_file): # not overwrite
        cache_file = os.path.join(cache_path, 'epoch{}_{}_{}.json'.format(start_epoch, "test-dev2017" if conf.testdev else "test2017",
                                  time.strftime("%Y-%m-%d_%H:%M", time.localtime())))
    with open(cache_file, 'w') as f:
        try:
            # pdb.set_trace()
            json.dump(vr, f)
            print("save test results in {}".format(cache_file))
        except OSError as err:
            pdb.set_trace()
            print('Could not save results: {}'.format(err))
            

def val_batch(batch_num, b):
    result = detector[b]
    if result is None:
        return np.zeros((0, 7))
    scores_np = result.obj_scores.data.cpu().numpy()
    cls_preds_np = result.obj_preds.data.cpu().numpy()
    boxes_np = result.boxes_assigned.data.cpu().numpy()
    im_inds_np = result.im_inds.data.cpu().numpy()  #is it???
    im_scales = b.im_sizes.reshape((-1, 3))[:, 2]

    #    if conf.dataset == 'coco':
    boxes_np /= im_scales[im_inds_np][:, None]
    boxes_np[:, 2:4] = boxes_np[:, 2:4] - boxes_np[:, 0:2] + 1
    cls_preds_np[:] = [test.ind_to_id[c_ind] for c_ind in cls_preds_np]
    # im_inds_np[:] = [test.ids[im_ind + batch_num * conf.batch_size * conf.num_gpus]
    #                  for im_ind in im_inds_np]
    # -----------------------
    instance_results = {"image_id": im_inds_np,
                        "category_id": cls_preds_np,
                        "bbox": boxes_np,
                        "score": scores_np}
    return instance_results


print("{}th Epoch Testing starts now!".format(start_epoch))

time_start = time.time()
val_epoch()
time_end = time.time()
h = (time_end - time_start) // 3600
m = (time_end - time_start - 3600 * h) // 60
s = (time_end - time_start) % 60
print("Cost {:2d}h{:2d}m{:2d}s for test epoch {}\n".format( int(h), int(m), int(s), start_epoch))
print("Model {} Test Done!".format(conf.ckpt))
print("Test on {} DONE!".format('coco2017' if conf.coco2017 else 'coco2014'))

