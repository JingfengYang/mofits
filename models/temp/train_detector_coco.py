"""
Training script 4 Detection
"""
import sys
import os
o_path = os.getcwd()
sys.path.append(o_path)

from dataloaders.mscoco import CocoDetection, CocoDataLoader
from dataloaders.msvoc import PascalDetection, PascalDataLoader
#from lib.object_detector import ObjectDetector
from lib.object_detector_roialgin import ObjectDetector
# from lib.object_detector_lsc import ObjectDetector_lsc
from lib.object_detector_stn import ObjectDetector_stn
from lib.object_detector_2 import ObjectDetector_stn2
#from lib.rfn_detector import RFNDetector
import numpy as np
from torch import optim
import torch
import pandas as pd
import time
from config import ModelConfig, FG_FRACTION, RPN_FG_FRACTION, IM_SCALE, BOX_SCALE
from torch.nn import functional as F
from lib.fpn.box_utils import bbox_loss
import torch.backends.cudnn as cudnn
from pycocotools.cocoeval import COCOeval
from lib.pytorch_misc import optimistic_restore, clip_grad_norm
from torch.optim.lr_scheduler import ReduceLROnPlateau
from tqdm import tqdm
import dill as pkl
from torch.autograd import Variable
from lib.focalloss import FocalLoss
import pdb

os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"   # see issue #152
os.environ["CUDA_VISIBLE_DEVICES"]="1"

cudnn.benchmark = True
conf = ModelConfig()

pdb.set_trace()

if conf.dataset == 'coco':
    if conf.coco2017:
        datasettype = 'coco2017'
    else:
        datasettype = 'coco2014'

    if conf.stn is not None:
        if conf.fpn:
            save_subdir = '{}_{}_fpn_stn_{}/detector'.format(datasettype, conf.model, conf.stn)
        else:
            save_subdir = '{}_{}_stn_{}/detector'.format(datasettype, conf.model, conf.stn)
    elif conf.klloss:
        save_subdir = '{}_{}_klloss/detector'.format(datasettype, conf.model)
    else:
        save_subdir = '{}_{}_roialign/detector'.format(datasettype, conf.model)

    if not os.path.exists(os.path.join(conf.save_dir, save_subdir)):
        os.makedirs(os.path.join(conf.save_dir, save_subdir))

    train, val = CocoDetection.splits(conf.coco2017)
    val.ids = val.ids[:conf.val_size]
    #val.ids = val.ids[10000:20000]
    train.ids = train.ids
    train_loader, val_loader = CocoDataLoader.splits(train, val, batch_size=conf.batch_size,
                                                     num_workers=conf.num_workers,
                                                     num_gpus=conf.num_gpus)

else:
    if conf.stn is not None:
        if conf.fpn:
            save_subdir = '{}_{}_fpn_stn_{}/detector'.format(conf.dataset, conf.model, conf.stn)
        else:
            save_subdir = '{}_{}_stn_{}/detector'.format(conf.dataset, conf.model, conf.stn)
    elif conf.klloss:
        save_subdir = '{}_{}_klloss/detector'.format(conf.dataset, conf.model)
    else:
        save_subdir = '{}_{}_roialign/detector'.format(conf.dataset, conf.model)

    if not os.path.exists(os.path.join(conf.save_dir, save_subdir)):
        os.makedirs(os.path.join(conf.save_dir, save_subdir))

    datasettype = conf.dataset
    train, val= PascalDetection.splits(datasettype)
    
    # val.ids = val.ids[:conf.val_size]
    # pdb.set_trace()
    train.ids = train.ids

    train_loader, val_loader = PascalDataLoader.splits(train, val, batch_size=conf.batch_size,
                                                     num_workers=conf.num_workers,
                                                     num_gpus=conf.num_gpus)


#if conf.model == 'rfn':
#    detector = RFNDetector(classes=train.ind_to_classes, num_gpus=conf.num_gpus,
#                              mode='rpntrain' if not conf.use_proposals else 'proposals', use_resnet=conf.use_resnet)
#else:

#if conf.lsc:
#    detector = ObjectDetector_lsc(classes=train.ind_to_classes, num_gpus=conf.num_gpus,
#                              mode='rpntrain' if not conf.use_proposals else 'proposals', use_resnet=conf.use_resnet)
if conf.stn is not None:
    detector = ObjectDetector_stn(classes=train.ind_to_classes, num_gpus=conf.num_gpus,
                              mode='rpntrain' if not conf.use_proposals else 'proposals', use_resnet=conf.use_resnet, thresh=conf.thresh)
else:
    detector = ObjectDetector_stn2(classes=train.ind_to_classes, num_gpus=conf.num_gpus,
                              mode='rpntrain' if not conf.use_proposals else 'proposals', use_resnet=conf.use_resnet, thresh=conf.thresh)

# Note: if you're doing the stanford setup, you'll need to change this to freeze the lower layers
if conf.use_proposals:
    for n, param in detector.named_parameters():
        if n.startswith('features'):
            param.requires_grad = False

optimizer = optim.SGD([p for p in detector.parameters() if p.requires_grad],
                      weight_decay=conf.l2, lr=conf.lr * conf.num_gpus * conf.batch_size, momentum=0.9)
scheduler = ReduceLROnPlateau(optimizer, 'max', patience=3, factor=0.1,
                              verbose=True, threshold=0.001, threshold_mode='abs', cooldown=1)

start_epoch = -1
if conf.ckpt is not None:
    ckpt = torch.load(conf.ckpt)
    if optimistic_restore(detector, ckpt['state_dict']):
        start_epoch = ckpt['epoch']
        # optimizer = ckpt['optimizer']

    print ("=> loaded checkpoint '{}' (train for {} epochs)".format(conf.ckpt,conf.num_epochs))
    ckpt = None

detector.cuda()



def train_epoch(epoch_num):
    detector.train()
    tr = []
    start = time.time()
    with tqdm(total=train_loader.__len__()) as pbar:    
        for b, batch in enumerate(train_loader):
            tr.append(train_batch(batch))
 
            if b % conf.print_interval == 0 and b >= conf.print_interval:
                mn = pd.concat(tr[-conf.print_interval:], axis=1).mean(1)
                time_per_batch = (time.time() - start) / conf.print_interval
                print("\ne{:2d}b{:5d}/{:5d} {:.3f}s/batch, {:.1f}m/epoch".format(
                    epoch_num, b, len(train_loader), time_per_batch, len(train_loader) * time_per_batch / 60))
                print(mn)
                pbar.write('-----------')
                start = time.time()
            pbar.update(1)
    return pd.concat(tr, axis=1)


def train_batch(b):
    """
    :param b: contains:
          :param imgs: the image, [batch_size, 3, IM_SIZE, IM_SIZE]
          :param all_anchors: [num_anchors, 4] the boxes of all anchors that we'll be using
          :param all_anchor_inds: [num_anchors, 2] array of the indices into the concatenated
                                  RPN feature vector that give us all_anchors,
                                  each one (img_ind, fpn_idx)
          :param im_sizes: a [batch_size, 4] numpy array of (h, w, scale, num_good_anchors) for each image.

          :param num_anchors_per_img: int, number of anchors in total over the feature pyramid per img

          Training parameters:
          :param train_anchor_inds: a [num_train, 5] array of indices for the anchors that will
                                    be used to compute the training loss (img_ind, fpn_idx)
          :param gt_boxes: [num_gt, 4] GT boxes over the batch.
          :param gt_classes: [num_gt, 2] gt boxes where each one is (img_id, class)

    :return:
    """
    result = detector[b]
    scores = result.od_obj_dists
    box_deltas = result.od_box_deltas
    labels = result.od_obj_labels
    roi_boxes = result.od_box_priors
    bbox_targets = result.od_box_targets
    rpn_scores = result.rpn_scores
    rpn_box_deltas = result.rpn_box_deltas

    # detector loss
    valid_inds = (labels.data != 0).nonzero().squeeze(1)
    fg_cnt = valid_inds.size(0)
    bg_cnt = labels.size(0) - fg_cnt
   
 #   if conf.fcloss:
 #       class_loss = FocalLoss(len(train.ind_to_classes), gamma=2)(scores, labels)
 #   else:
    class_loss = F.cross_entropy(scores, labels)

    # No gather_nd in pytorch so instead convert first 2 dims of tensor to 1d
    box_reg_mult = 2 * (1. / FG_FRACTION) * fg_cnt / (fg_cnt + bg_cnt + 1e-4)
    twod_inds = valid_inds * box_deltas.size(1) + labels[valid_inds].data

    box_loss = bbox_loss(roi_boxes[valid_inds], box_deltas.view(-1, 4)[twod_inds],
                         bbox_targets[valid_inds],conf.klloss) * box_reg_mult

    loss = class_loss + box_loss

    # RPN loss
    if not conf.use_proposals:
        train_anchor_labels = b.train_anchor_labels[:, -1]
        train_anchors = b.train_anchors[:, :4]
        train_anchor_targets = b.train_anchors[:, 4:]

        train_valid_inds = (train_anchor_labels.data == 1).nonzero().squeeze(1)
        #pdb.set_trace()

        if conf.fcloss:
            rpn_class_loss = FocalLoss(class_num=2, gamma=2)(rpn_scores, train_anchor_labels)
        else:
            rpn_class_loss = F.cross_entropy(rpn_scores, train_anchor_labels)

        # print("{} fg {} bg, ratio of {:.3f} vs {:.3f}. RPN {}fg {}bg ratio of {:.3f} vs {:.3f}".format(
        #     fg_cnt, bg_cnt, fg_cnt / (fg_cnt + bg_cnt + 1e-4), FG_FRACTION,
        #     train_valid_inds.size(0), train_anchor_labels.size(0)-train_valid_inds.size(0),
        #     train_valid_inds.size(0) / (train_anchor_labels.size(0) + 1e-4), RPN_FG_FRACTION), flush=True)
        rpn_box_mult = 2 * (1. / RPN_FG_FRACTION) * train_valid_inds.size(0) / (train_anchor_labels.size(0) + 1e-4)
        rpn_box_loss = bbox_loss(train_anchors[train_valid_inds],
                                 rpn_box_deltas[train_valid_inds],
                                 train_anchor_targets[train_valid_inds]) * rpn_box_mult

        loss += rpn_class_loss + rpn_box_loss
        #(torch.__version__ == '0.4.0'):
#        res = pd.Series([rpn_class_loss.item(), rpn_box_loss.item(),
#                         class_loss.item(), box_loss.item(), loss.item()],
#                        ['rpn_class_loss', 'rpn_box_loss', 'class_loss', 'box_loss', 'total'])
        res = pd.Series([rpn_class_loss.data[0], rpn_box_loss.data[0],
                         class_loss.data[0], box_loss.data[0], loss.data[0]],
                        ['rpn_class_loss', 'rpn_box_loss', 'class_loss', 'box_loss', 'total'])

    else:
        #(torch.__version__ == '0.4.0')
#        res = pd.Series([class_loss.item(), box_loss.item(), loss.item()],
#                        ['class_loss', 'box_loss', 'total'])
        res = pd.Series([class_loss.data[0], box_loss.data[0], loss.data[0]],
                        ['class_loss', 'box_loss', 'total'])

    optimizer.zero_grad()
    loss.backward()
    clip_grad_norm(
        [(n, p) for n, p in detector.named_parameters() if p.grad is not None],
        max_norm=conf.clip, clip=True)
    optimizer.step()

    return res


def val_epoch():
    detector.eval()
    # all_boxes is a list of length number-of-classes.
    # Each list element is a list of length number-of-images.
    # Each of those list elements is either an empty list []
    # or a numpy array of detection.
    if conf.stn is not None:
        if conf.fpn:
            cache_file = os.path.join(conf.cache, 'stn/detector_{}_fpn_stn{}_epoch{}_{}.pkl'.format(datasettype, conf.stn, epoch,
                                                                                        'val' if not conf.test else 'test'))
        else:
            cache_file = os.path.join(conf.cache, 'stn/detector_{}_stn{}_epoch{}_{}.pkl'.format(datasettype, conf.stn, epoch,
                                                                                        'val' if not conf.test else 'test'))
    else:
        cache_file = os.path.join(conf.cache, 'stn/detector_{}_epoch{}_{}.pkl'.format(datasettype, epoch,
                                                                                    'val' if not conf.test else 'test'))
    if os.path.isfile(cache_file):
        # vr = np.load(cache_file)
        with open(cache_file, 'rb') as f:
            vr = pkl.load(f)
    else:
        vr = []
        with tqdm(total=val_loader.__len__()) as pbar:
            for val_b, batch in enumerate(val_loader):
                vr.append(val_batch(val_b, batch))
                pbar.update(1)
        vr = np.concatenate(vr, 0)
        with open(cache_file, 'wb') as f:
            try:
                pkl.dump(vr, f)
                print("save evaluation results in {}".format(cache_file))
            except OSError as err:
                print('Could not save results: {}'.format(err))
        np.save("test.npy", vr)

    if vr.shape[0] == 0:
        print("No detections anywhere")
        return 0.0

    val_coco = val.coco
    coco_dt = val_coco.loadRes(vr)
    coco_eval = COCOeval(val_coco, coco_dt, 'bbox')

#    coco_eval.params.imgIds = val.ids if conf.coco else [x for x in range(len(val))]
    coco_eval.params.imgIds = val.ids

    coco_eval.evaluate()
    coco_eval.accumulate()
    coco_eval.summarize()
    mAp = coco_eval.stats[1]
    return mAp


def val_batch(batch_num, b):
    result = detector[b]
    if result is None:
        return np.zeros((0, 7))
    scores_np = result.obj_scores.data.cpu().numpy()
    cls_preds_np = result.obj_preds.data.cpu().numpy()
    boxes_np = result.boxes_assigned.data.cpu().numpy()
    im_inds_np = result.im_inds.data.cpu().numpy()
    im_scales = b.im_sizes.reshape((-1, 3))[:, 2]

#    if conf.dataset == 'coco':
    boxes_np /= im_scales[im_inds_np][:, None]
    boxes_np[:, 2:4] = boxes_np[:, 2:4] - boxes_np[:, 0:2] + 1
    cls_preds_np[:] = [val.ind_to_id[c_ind] for c_ind in cls_preds_np]

    im_inds_np[:] = [val.ids[im_ind + batch_num * conf.batch_size * conf.num_gpus]
                     for im_ind in im_inds_np]
#-----------------------

    return np.column_stack((im_inds_np, boxes_np, scores_np, cls_preds_np))


best_mAp = -1

for epoch in range(start_epoch + 1, start_epoch + 1 + conf.num_epochs):

    if not conf.test:
        print("{}th Epoch Training starts now!".format(epoch))
        time_start = time.time()
        rez = train_epoch(epoch)
        time_end = time.time()
        h = (time_end-time_start)//3600
        m = (time_end-time_start-3600*h)//60
        s = (time_end-time_start) % 60
        print("overall{:2d}: ({:.3f})\n{}".format(\
            epoch, rez.mean(1)['total'], rez.mean(1)), flush=True)
        print("Cost {:2d}h{:2d}m{:2d}s for training epoch {}\n".format(\
            int(h), int(m), int(s), epoch))
#        if epoch % 3 == 0 and epoch > start_epoch + 1:
        torch.save({
            'epoch': epoch,
            'state_dict': detector.state_dict(),
            'optimizer': optimizer.state_dict(),
        }, os.path.join(conf.save_dir,save_subdir, '{}-detector-{}.tar'.format(datasettype, epoch)))
        print("Save model {}".format(os.path.join(conf.save_dir, save_subdir,  '{}-detector-{}.tar'.format(datasettype, epoch))))

        if conf.noval:
            continue

    if conf.test:
        print("{}th Epoch Testing starts now!".format(epoch))
    else:
        print("{}th Epoch Valuating starts now!".format(epoch))

    time_start = time.time()
    mAp = val_epoch()
    scheduler.step(mAp)
    time_end = time.time()
    h = (time_end - time_start) // 3600
    m = (time_end - time_start - 3600 * h) // 60
    s = (time_end - time_start) % 60
    print("Epoch {} {} mAP={} (best mAp={})".format(
        epoch, "val" if not conf.test else "test", mAp * 100, best_mAp * 100))
    print("Cost {:2d}h{:2d}m{:2d}s for test epoch {}\n".format(
        int(h), int(m), int(s), epoch))

    if not conf.test:
        if mAp > best_mAp:
            best_mAp = mAp
            torch.save({
                'epoch': epoch,
                'state_dict': detector.state_dict(),
                'optimizer': optimizer.state_dict(),
            }, os.path.join(conf.save_dir, save_subdir, '{}-best_detector.tar'.format(datasettype)))
            print("Best mAp is updated: {:2f} in epoch {}".format(best_mAp*100, epoch))

    if conf.test:
        print("Model {} evaluation Done!".format(conf.ckpt))
        break

print("{} on coco DONE!".format("Test" if conf.test else "Training"))