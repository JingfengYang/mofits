#!/usr/bin/env bash
export PYTHONPATH=$(pwd)
# Train the model without COCO pretraining
#python models/train_detector_coco.py -b 8 -lr 1e-3 -nepoch 10 -ngpu 4 -nwork 2
#python models/train_detector_coco.py -b 8 -lr 1e-3 -nepoch 10 -ngpu 1 -nwork 2
#python models/train_detector_coco.py -b 2 -lr 1e-4 -nepoch 20 -ngpu 4 -nwork 2 -ckpt /home/jfyang/source/mofits/checkpoints/coco_rfn/detector/coco-best_detector.tar
#-ckpt /home/jfyang/source/mofits/checkpoints/coco_rfn/detector/coco-detector_0.tar

#python models/train_detector_coco.py -b 1 -lr 1e-3 -nepoch 10 -ngpu 1 -nwork 1


python models/test_detector_coco.py -b 4 -lr 1e-4 -nepoch 30 -ngpu 4 -nwork 2 -fcloss -stn 6 -ckpt /home/jfyang/source/mofits/checkpoints/coco2017_faster_rcnn_stn_6/detector/coco2017-detector-19.tar

python models/train_detector_coco.py -b 4 -lr 1e-4 -nepoch 30 -ngpu 4 -nwork 2 -fcloss -stn 6 -coco2017 -noval
