import json
import os

# train dataset = train + val
# test dataset = test


#save

json_path = '/home/jfyang/datasets/VOCdevkit/PASCAL_VOC'


json_train07 = json.load(open( json_path + '/pascal_train2007.json', 'r'))
json_train12 = json.load(open(json_path + '/pascal_train2012.json', 'r'))
json_val07 = json.load(open(json_path + '/pascal_val2007.json', 'r'))
json_val12 = json.load(open(json_path + '/pascal_val2012.json', 'r'))




# pascal voc 07
json_trainval07 = {"images": json_train07['images']+json_val07['images'],
                           "type": "instances",
                           "annotations": json_train07['annotations']+json_val07['annotations'],
                           "categories": json_train07['categories']}
filename = 'pascal_trainval07.json'
with open(os.path.join(json_path, filename), 'w') as f:
            f.write(json.dumps(json_trainval07))

# pascal voc 12
json_trainval12 = {"images": json_train12['images']+json_val12['images'],
                           "type": "instances",
                           "annotations": json_train12['annotations']+json_val12['annotations'],
                           "categories": json_train12['categories']}

filename = 'pascal_trainval12.json'
with open(os.path.join(json_path, filename), 'w') as f:
            f.write(json.dumps(json_trainval12))

# pascal voc 07 + 12

json_trainval0712 = {"images": json_trainval07['images']+json_trainval12['images'],
                           "type": "instances",
                           "annotations": json_trainval07['annotations']+json_trainval12['annotations'],
                           "categories": json_trainval07['categories']}

filename = 'pascal_trainval0712.json'
with open(os.path.join(json_path, filename), 'w') as f:
            f.write(json.dumps(json_trainval0712))
