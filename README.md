
# Setup


0. Install python3.6 and pytorch 
 ```conda install pytorch=0.4.0 torchvision cuda90 -c pytorch```.

1. Requirement list

	cython
	cffi
	opencv-python
	scipy
	msgpack
	easydict
	matplotlib
	pyyaml
	tensorboardX
	shapely
	pycocotools
	cython
	tdqm
	pandas
	dill
	h5py
	numpy=1.16.0

2. Update the config file with the dataset paths. Specifically:
    - You'll also need to fix your PYTHONPATH: ```export PYTHONPATH=/home/rowan/code/scene-graph``` 

3. Compile everything. run ```make``` in the main directory: this compiles the Bilinear Interpolation operation for the RoIs as well as the Highway LSTM.

